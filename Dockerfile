FROM openjdk:11
EXPOSE 7070
ARG JAR_FILE=target/etaskify-0.1.0.jar
ADD ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
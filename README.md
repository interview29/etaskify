# eTaskify
eTaskify, is a cloud based enterprise task manager platform for organizations where companies can manage their daily tasks online.

# Deployment
- mvn clean package
- docker build -t etaskify .
- docker run -d -p 7070:7070 etaskify


# APIs description
## Auth
- POST /auth-management/v1/sign-up  - to sign up to system. It will create organization and admin user.
- POST /auth-management/v1/sign-in  - to sign in to system. 

Below operations requries Bearer authentication.

## Tasks
- POST task-management/v1/tasks               - to create a task and assign to users. In order to run this command user has to have admin role.
- GET  task-management/v1/tasks                - to get only own tasks. User will get only his/her tasks.
- GET  task-management/v1/organization-tasks  - to get all tasks of organization. Only admins of organizations' can see these tasks

## Users
- POST /user-management/v1/users - the admin of organization can create user in this company



# More documentation can be found in swagger
http://localhost:7070/swagger-ui.html

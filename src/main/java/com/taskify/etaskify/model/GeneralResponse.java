package com.taskify.etaskify.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author : Orkhan Namazov
 * @since : 12.09.2021
 **/

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GeneralResponse {
    private String result;
    private String message;
}

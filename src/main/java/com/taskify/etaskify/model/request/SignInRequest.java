package com.taskify.etaskify.model.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * @author : Orkhan Namazov
 * @since : 11.09.2021
 **/

@Data
public class SignInRequest {
    @NotBlank(message = "Username cannot be blank")
    private String usernameOrEmail;

    @NotBlank(message = "Password cannot be blank")
    @Size(min = 6, message = "Password must contain at least 6 characters")
    private String password;
}
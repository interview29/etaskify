package com.taskify.etaskify.model.dto;

import lombok.Data;

/**
 * @author : Orkhan Namazov
 * @since : 12.09.2021
**/

@Data
public class EmailDto {
    private String subject;
    private String body;
    private String to;
}

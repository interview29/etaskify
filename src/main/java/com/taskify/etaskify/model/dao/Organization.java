package com.taskify.etaskify.model.dao;

import lombok.*;

/**
 * @author : Orkhan Namazov
 * @since : 11.09.2021
 **/

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
public class Organization {
    private long id;
    @NonNull
    private String name;
    @NonNull
    private String phoneNumber;
    @NonNull
    private String address;
}

package com.taskify.etaskify.model.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.time.LocalDateTime;

/**
 * @author : Orkhan Namazov
 * @since : 11.09.2021
 **/

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Task {
    private long id;
    @NonNull
    private String title;

    @NonNull
    private String description;

    @NonNull
    private LocalDateTime deadline;

    @NonNull
    private TaskStatus status;

    private long organizationId;
}

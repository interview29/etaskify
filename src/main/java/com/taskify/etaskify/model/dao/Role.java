package com.taskify.etaskify.model.dao;

/**
 * @author : Orkhan Namazov
 * @since : 11.09.2021
 **/

public enum Role {
    ROLE_ADMIN,
    ROLE_USER
}

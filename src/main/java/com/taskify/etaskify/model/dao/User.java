package com.taskify.etaskify.model.dao;

import lombok.Data;

/**
 * @author : Orkhan Namazov
 * @since : 11.09.2021
 **/

@Data
public class User {
    private long id;
    private String username;
    private String email;
    private String name;
    private String surname;
    private String password;
    private Role role;
    private long organizationId;
    private Boolean active;
    private Boolean credentialsNonExpired;
    private Boolean accountNonLocked;
    private Boolean accountNonExpired;

}

package com.taskify.etaskify.controller;

import com.taskify.etaskify.model.GeneralResponse;
import com.taskify.etaskify.model.dto.UserDto;
import com.taskify.etaskify.model.request.SignInRequest;
import com.taskify.etaskify.model.request.SignUpRequest;
import com.taskify.etaskify.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author : Orkhan Namazov
 * @since : 11.09.2021
 **/

@RestController
@RequestMapping("auth-management/v1")
@RequiredArgsConstructor
public class AuthController {
    private final AuthService authService;


    //SignUp
    @PostMapping("/sign-up")
    public ResponseEntity signUp(@Valid @RequestBody SignUpRequest request) {
        int result = authService.signUp(request);
        return result == 0 ?
                ResponseEntity.badRequest().build() :
                ResponseEntity.ok(new GeneralResponse("Success", "User and Organization Created!"));
    }

    //SignIn
    @PostMapping("/sign-in")
    public ResponseEntity signIn(@Valid @RequestBody SignInRequest request) {
        UserDto userDto = authService.signIn(request);
        return (userDto.getToken() == null || userDto.getToken().isBlank())
                ? ResponseEntity.notFound().build()
                : ResponseEntity.ok(userDto);
    }

}

package com.taskify.etaskify.controller;

import com.taskify.etaskify.model.GeneralResponse;
import com.taskify.etaskify.model.request.UserCreationRequest;
import com.taskify.etaskify.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author : Orkhan Namazov
 * @since : 11.09.2021
 **/

@RestController
@RequestMapping("user-management/v1")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @PostMapping("/users")
    @Secured("ROLE_ADMIN")
    public ResponseEntity createUser(@Valid @RequestBody UserCreationRequest request) {
        userService.createUser(request);
        return ResponseEntity.ok(
                new GeneralResponse("Success", "User Created Successfully"));
    }


}

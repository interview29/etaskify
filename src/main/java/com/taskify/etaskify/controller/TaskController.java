package com.taskify.etaskify.controller;

import com.taskify.etaskify.model.GeneralResponse;
import com.taskify.etaskify.model.request.TaskCreationRequest;
import com.taskify.etaskify.service.TaskService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author : Orkhan Namazov
 * @since : 11.09.2021
 **/


@RestController
@RequestMapping("task-management/v1/")
@RequiredArgsConstructor
public class TaskController {
    private final TaskService taskService;

    @PostMapping("/tasks")
    public ResponseEntity createTask(@Valid @RequestBody TaskCreationRequest request) {
        taskService.createTask(request);
        return ResponseEntity.ok(new GeneralResponse("Success", "Created Successfully"));
    }

    @GetMapping("/organization-tasks")
    public ResponseEntity getOrganizationTasks() {
        return ResponseEntity.ok(taskService.getOrganizationsTasks());
    }

    @GetMapping("/tasks")
    public ResponseEntity getUserTasks() {
        return ResponseEntity.ok(taskService.getUsersTasks());
    }

}

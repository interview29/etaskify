package com.taskify.etaskify.repository;

import com.taskify.etaskify.model.dao.Task;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author : Orkhan Namazov
 * @since : 11.09.2021
 **/

@Mapper
public interface TaskRepository {

    @Select("SELECT * FROM tasks where id = #{taskId}")
    Task getTaskById(long taskId);

    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    @Insert("INSERT INTO TASKS (title, description, deadline, status, organization_id) " +
            "VALUES (#{title}, #{description}, #{deadline}, #{status}, #{organizationId} )")
    int createTask(Task task);


    @Insert({
            "<script>",
            "INSERT INTO USERS_TASKS",
            "(user_id, task_id)",
            "VALUES" +
            "<foreach item='userId' collection='userIds' open='' separator=',' close=''>" +
            "(#{userId}, #{taskId})" +
            "</foreach>",
            "</script>"})
    int assignTaskToUser(long taskId, List<Long> userIds);


    @Select("SELECT * FROM tasks where organization_id = #{organizationId}")
    List<Task> getTasksOfOrganization(long organizationId);

    @Select("SELECT t.* FROM TASKS t INNER JOIN USERS_TASKS ut ON t.id = ut.task_id where ut.user_id = #{userId}")
    List<Task> getTasksOfUser(long userId);

}

package com.taskify.etaskify.repository;

import com.taskify.etaskify.model.dao.Organization;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

/**
 * @author : Orkhan Namazov
 * @since : 11.09.2021
 **/

@Mapper
public interface OrganizationRepository {
    @Select("SELECT * FROM organizations WHERE id = #{id}")
    Organization getById(long id);

    @Select("SELECT o.* FROM USERS u INNER JOIN ORGANIZATIONS o ON u.organization_id = o.id WHERE  u.id = #{userId}")
    Organization getByUserId(long userId);

    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    @Insert("INSERT INTO organizations(name, phone_number, address) " +
            " VALUES (#{name}, #{phoneNumber}, #{address})")
    int insert(Organization organization);
}

package com.taskify.etaskify.repository;

import com.taskify.etaskify.model.dao.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author : Orkhan Namazov
 * @since : 11.09.2021
 **/

@Mapper
public interface UserRepository {
    @Select({
            "<script>",
            "select * FROM USERS",
            "WHERE  ID IN  " +
                    "<foreach item='item' collection='ids' open='(' separator=',' close=')'> #{item} </foreach>" +
                    "</script>"  })
    List<User> findUsersById(List<Long> ids);


    @Select("SELECT * FROM USERS WHERE username = #{username} or email= #{username}")
    User findByUsername(String username);

    @Select("SELECT * FROM USERS WHERE organization_id = #{organizationId}")
    List<User> findUsersOfOrganization(long organizationId);

    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    @Insert("INSERT INTO USERS (username, email, name, surname, password, role, organization_id) " +
            "VALUES (#{username}, #{email}, #{name}, #{surname}, #{password}, #{role}, #{organizationId})")
    int insert(User user);

    @Select("SELECT count(0) FROM USERS WHERE username = #{username}")
    int checkUsername(String username);

    @Select("SELECT count(0) FROM USERS WHERE email = #{email}")
    int checkEmail(String email);


}

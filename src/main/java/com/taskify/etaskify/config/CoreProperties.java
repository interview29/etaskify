package com.taskify.etaskify.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author : Orkhan Namazov
 * @since : 11.09.2021
 **/


@ConfigurationProperties(value = "core")
@Configuration
@Data
public class CoreProperties
{
    private String jwtSecret;
}
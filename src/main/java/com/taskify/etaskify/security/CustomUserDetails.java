package com.taskify.etaskify.security;

import com.taskify.etaskify.model.dao.User;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

/**
 * @author : Orkhan Namazov
 * @since : 12.09.2021
 **/



public class CustomUserDetails implements UserDetails
{
    @Getter
    @Setter
    private final User user;

    private final Collection<? extends GrantedAuthority> grantedAuthorities;

    public CustomUserDetails(User user, Collection<? extends GrantedAuthority> grantedAuthorities)
    {
        this.user = user;
        this.grantedAuthorities = grantedAuthorities;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities()
    {
        return grantedAuthorities;
    }

    @Override
    public String getPassword()
    {
        return user.getPassword();
    }

    @Override
    public String getUsername()
    {
        return user.getUsername();
    }

    @Override
    public boolean isAccountNonExpired()
    {
        return user.getAccountNonExpired();
    }

    @Override
    public boolean isAccountNonLocked()
    {
        return user.getAccountNonLocked();
    }

    @Override
    public boolean isCredentialsNonExpired()
    {
        return user.getCredentialsNonExpired();
    }

    @Override
    public boolean isEnabled()
    {
        return user.getActive();
    }
}
package com.taskify.etaskify.exception;

import com.taskify.etaskify.model.GeneralResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDate;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author : Orkhan Namazov
 * @since : 12.09.2021
 **/

@ControllerAdvice
public class ControllerAdvisor extends ResponseEntityExceptionHandler {

    @ExceptionHandler(UserExistException.class)
    public ResponseEntity handleUserExistException(UserExistException ex) {
        return ResponseEntity
                .status(ex.getHttpStatus())
                .body(new GeneralResponse("Failure", ex.getMessage()));
    }

    @ExceptionHandler(TaskAssignException.class)
    public ResponseEntity handleTaskAssignException(TaskAssignException ex) {
        return ResponseEntity
                .status(ex.getHttpStatus())
                .body(new GeneralResponse("Failure", ex.getMessage()));
    }

    @ExceptionHandler(InsufficientRoleException.class)
    public ResponseEntity handleInsufficientRoleException(InsufficientRoleException ex) {
        return ResponseEntity
                .status(ex.getHttpStatus())
                .body(new GeneralResponse("Failure", ex.getMessage()));
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDate.now());
        body.put("status", status.value());

        List<String> errors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(x -> x.getDefaultMessage())
                .collect(Collectors.toList());

        body.put("errors", errors);

        return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
    }
}

package com.taskify.etaskify.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

/**
 * @author : Orkhan Namazov
 * @since : 12.09.2021
 **/

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaskAssignException extends RuntimeException {
    private HttpStatus httpStatus;
    private String message;
}

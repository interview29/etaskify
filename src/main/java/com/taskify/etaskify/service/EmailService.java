package com.taskify.etaskify.service;

import com.taskify.etaskify.model.dto.EmailDto;

/**
 * @author : Orkhan Namazov
 * @since : 11.09.2021
 **/

public interface EmailService {

    void send(EmailDto email);

}

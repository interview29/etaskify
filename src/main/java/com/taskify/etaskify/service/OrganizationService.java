package com.taskify.etaskify.service;

import com.taskify.etaskify.model.dao.Organization;

/**
 * @author : Orkhan Namazov
 * @since : 11.09.2021
 **/

public interface OrganizationService {
    int createOrganization(Organization organization);

    Organization getOrganizationByUser();

}

package com.taskify.etaskify.service.impl;

import com.taskify.etaskify.repository.UserRepository;
import com.taskify.etaskify.service.AvailabilityChecker;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author : Orkhan Namazov
 * @since : 12.09.2021
 **/

@Slf4j
@Service
@RequiredArgsConstructor
public class AvailabilityCheckerImpl implements AvailabilityChecker {
    private final UserRepository userRepository;

    @Override
    public boolean isUsernameAvailable(String username) {
        log.info("Checking username");
        return userRepository.checkUsername(username) == 0;
    }

    @Override
    public boolean isEmailAvailable(String email) {
        log.info("Checking email");
        return userRepository.checkEmail(email) == 0;
    }
}

package com.taskify.etaskify.service.impl;

import com.taskify.etaskify.model.dao.Organization;
import com.taskify.etaskify.model.dao.User;
import com.taskify.etaskify.repository.OrganizationRepository;
import com.taskify.etaskify.security.CustomUserDetails;
import com.taskify.etaskify.security.CustomUserDetailsService;
import com.taskify.etaskify.service.OrganizationService;
import com.taskify.etaskify.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

/**
 * @author : Orkhan Namazov
 * @since : 11.09.2021
 **/

@Slf4j
@Service
@RequiredArgsConstructor
public class OrganizationServiceImpl implements OrganizationService {
    private final OrganizationRepository organizationRepository;
    private final CustomUserDetailsService userService;

    @Override
    public int createOrganization(Organization organization) {
        log.info("Creating Organization");
        return  organizationRepository.insert(organization);
    }

    @Override
    public Organization getOrganizationByUser() {
        log.info("Getting organization of User {}");
        User user = userService.getUser();
        return organizationRepository.getByUserId(user.getId());
    }


}

package com.taskify.etaskify.service.impl;

import com.taskify.etaskify.exception.TaskAssignException;
import com.taskify.etaskify.model.dao.Organization;
import com.taskify.etaskify.model.dao.Task;
import com.taskify.etaskify.model.dao.TaskStatus;
import com.taskify.etaskify.model.dao.User;
import com.taskify.etaskify.model.dto.EmailDto;
import com.taskify.etaskify.model.request.TaskCreationRequest;
import com.taskify.etaskify.repository.TaskRepository;
import com.taskify.etaskify.security.CustomUserDetailsService;
import com.taskify.etaskify.service.EmailService;
import com.taskify.etaskify.service.OrganizationService;
import com.taskify.etaskify.service.TaskService;
import com.taskify.etaskify.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author : Orkhan Namazov
 * @since : 12.09.2021
 **/

@Slf4j
@Service
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService {
    private final TaskRepository taskRepository;
    private final OrganizationService organizationService;
    private final UserService userService;
    private final CustomUserDetailsService userDetailsService;
    private final EmailService emailService;

    @Override
    public Task getTaskById(long id) {
        log.info("Getting task id={}", id);
        return taskRepository.getTaskById(id);
    }

    @Override
    public void createTask(TaskCreationRequest request) {
        log.info("Creating task {}", request);
        Task task = new Task();
        task.setTitle(request.getTitle());
        task.setDescription(request.getDescription());
        task.setDeadline(request.getDeadline());
        task.setStatus(TaskStatus.NEW);
        long organizationId = organizationService.getOrganizationByUser().getId();
        task.setOrganizationId(organizationId);

        taskRepository.createTask(task);

        List<User> users = userService.getUsersById(request.getUserIds());


        List<Long> collect = users
                .stream()
                .filter(user -> user.getOrganizationId() == organizationId)
                .map(User::getId)
                .collect(Collectors.toList());

        assignTaskToUsers(task, collect);
    }

    @Override
    public int assignTaskToUsers(Task task, List<Long> userIds) {
        if (userIds == null || userIds.isEmpty())
            throw new TaskAssignException(HttpStatus.BAD_REQUEST, "Task cannot be assigned because user not found");

        log.info("Assign task(task={}) to users: {}", task.getId(), userIds);
        int result = taskRepository.assignTaskToUser(task.getId(), userIds);
        List<User> users = userService.getUsersById(userIds);

        if (result > 0) users.forEach(user -> sendEmailToUsers(task, user.getEmail()));
        log.info("Task {} assigned the users. count is ", task, result);
        return result;

    }

    @Override
    public List<Task> getOrganizationsTasks() {
        Organization organizationByUser = organizationService.getOrganizationByUser();
        log.info("Getting tasks of organization = {}", organizationByUser.getId());
        List<Task> tasksOfOrganization = taskRepository.getTasksOfOrganization(organizationByUser.getId());
        return tasksOfOrganization;
    }

    @Override
    public List<Task> getUsersTasks() {
        User user = userDetailsService.getUser();
        log.info("Getting tasks of organization = {}", user.getId());
        return taskRepository.getTasksOfUser(user.getId());
    }


    private void sendEmailToUsers(Task task, String emailTo) {

        EmailDto email = new EmailDto();
        email.setSubject("[New Task Alert!] New Task Assigned");
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("New Task assigned to you.");
        stringBuilder.append("\n\n\n");
        stringBuilder.append("Task Details:");

        stringBuilder.append("\n");
        stringBuilder.append("Task id: ");
        stringBuilder.append(task.getId());

        stringBuilder.append("\n");
        stringBuilder.append("Task title: ");
        stringBuilder.append(task.getTitle());

        stringBuilder.append("\n");
        stringBuilder.append("Task description: ");
        stringBuilder.append(task.getDescription());

        stringBuilder.append("\n");
        stringBuilder.append("Task deadline: ");
        stringBuilder.append(task.getDeadline());

        email.setBody(stringBuilder.toString());
        email.setTo(emailTo);

        emailService.send(email);
    }
}

package com.taskify.etaskify.service.impl;

import com.taskify.etaskify.exception.UserExistException;
import com.taskify.etaskify.model.dao.Organization;
import com.taskify.etaskify.model.dao.Role;
import com.taskify.etaskify.model.dao.User;
import com.taskify.etaskify.model.dto.UserDto;
import com.taskify.etaskify.model.request.SignInRequest;
import com.taskify.etaskify.model.request.SignUpRequest;
import com.taskify.etaskify.repository.UserRepository;
import com.taskify.etaskify.security.JwtUtil;
import com.taskify.etaskify.service.AuthService;
import com.taskify.etaskify.service.AvailabilityChecker;
import com.taskify.etaskify.service.OrganizationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * @author : Orkhan Namazov
 * @since : 11.09.2021
 **/

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {
    private final AuthenticationManager authenticationManager;
    private final JwtUtil jwtUtil;
    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final OrganizationService organizationService;
    private final AvailabilityChecker availabilityChecker;

    @Override
    public int signUp(SignUpRequest request) {
//        Check username and email.
        if (!availabilityChecker.isEmailAvailable(request.getEmail()))
            throw new UserExistException(HttpStatus.BAD_REQUEST, "This email already registered");
        if (!availabilityChecker.isUsernameAvailable(request.getUsername()))
            throw new UserExistException(HttpStatus.BAD_REQUEST, "This username already registered");

        // Create Organization
        Organization organization = new Organization(request.getOrganization(), request.getPhoneNumber(), request.getAddress());
        organizationService.createOrganization(organization);

        // Create User
        User user = new User();
        user.setEmail(request.getEmail());
        user.setUsername(request.getUsername());
        user.setPassword(passwordEncoder.encode(request.getPassword()));
        user.setRole(Role.ROLE_ADMIN);
        user.setOrganizationId(organization.getId());
        user.setActive(true);
        int insert = userRepository.insert(user);

        return insert;
    }

    @Override
    public UserDto signIn(SignInRequest request) {
        log.info("Sign in with {}", request.getUsernameOrEmail());
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                request.getUsernameOrEmail(),
                request.getPassword()
        ));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String token = jwtUtil.generateToken(authentication);
        return new UserDto(jwtUtil.extractUserId(token), request.getUsernameOrEmail(), token);
    }


}

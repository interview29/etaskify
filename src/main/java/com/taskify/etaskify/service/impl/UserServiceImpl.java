package com.taskify.etaskify.service.impl;

import com.taskify.etaskify.exception.UserExistException;
import com.taskify.etaskify.model.dao.Role;
import com.taskify.etaskify.model.dao.User;
import com.taskify.etaskify.model.request.UserCreationRequest;
import com.taskify.etaskify.repository.UserRepository;
import com.taskify.etaskify.security.CustomUserDetails;
import com.taskify.etaskify.service.AvailabilityChecker;
import com.taskify.etaskify.service.OrganizationService;
import com.taskify.etaskify.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author : Orkhan Namazov
 * @since : 12.09.2021
 **/

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final AvailabilityChecker availabilityChecker;
    private final OrganizationService organizationService;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;



    @Override
    public List<User> getUsersById(List<Long> ids) {
        log.info("Getting user info for {}", ids);
        return userRepository.findUsersById(ids);
    }

    @Override
    public Integer createUser(UserCreationRequest request) {
        log.info("Creating new user");
        //        Check username and email.
        if (!availabilityChecker.isEmailAvailable(request.getEmail()))
            throw new UserExistException(HttpStatus.BAD_REQUEST, "This email already registered");
        if (!availabilityChecker.isUsernameAvailable(request.getUsername()))
            throw new UserExistException(HttpStatus.BAD_REQUEST, "This username already registered");

        //        Check user is admin or not
/*        if (!getUser().getRole().equals(Role.ROLE_ADMIN))
            throw new InsufficientRoleException(HttpStatus.BAD_REQUEST, "User does not have admin role");*/

        User user = new User();
        user.setUsername(request.getUsername());
        user.setEmail(request.getEmail());
        user.setName(request.getName());
        user.setSurname(request.getSurname());
        user.setRole(Role.ROLE_USER);
        user.setPassword(passwordEncoder.encode(request.getPassword()));
        user.setOrganizationId(organizationService.getOrganizationByUser().getId());

        return userRepository.insert(user);
    }

}

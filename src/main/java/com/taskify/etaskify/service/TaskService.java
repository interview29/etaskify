package com.taskify.etaskify.service;

import com.taskify.etaskify.model.dao.Task;
import com.taskify.etaskify.model.request.TaskCreationRequest;

import java.util.List;

/**
 * @author : Orkhan Namazov
 * @since : 11.09.2021
 **/

public interface TaskService {
    Task getTaskById(long id);

    void createTask(TaskCreationRequest request);

    int assignTaskToUsers(Task task, List<Long> userIds);

    List<Task> getOrganizationsTasks();

    List<Task> getUsersTasks();

}

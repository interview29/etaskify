package com.taskify.etaskify.service;

import com.taskify.etaskify.model.dto.UserDto;
import com.taskify.etaskify.model.request.SignInRequest;
import com.taskify.etaskify.model.request.SignUpRequest;

/**
 * @author : Orkhan Namazov
 * @since : 11.09.2021
 **/

public interface AuthService {
    int signUp(SignUpRequest request);

    UserDto signIn(SignInRequest request);
}
package com.taskify.etaskify.service;

/**
 * @author : Orkhan Namazov
 * @since : 12.09.2021
 **/

public interface AvailabilityChecker {
    boolean isUsernameAvailable(String username);

    boolean isEmailAvailable(String email);
}

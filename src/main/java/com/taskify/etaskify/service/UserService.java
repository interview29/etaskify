package com.taskify.etaskify.service;

import com.taskify.etaskify.model.dao.User;
import com.taskify.etaskify.model.request.UserCreationRequest;

import java.util.List;

/**
 * @author : Orkhan Namazov
 * @since : 11.09.2021
 **/

public interface UserService {
    List<User> getUsersById(List<Long> ids);

    Integer createUser(UserCreationRequest request);

}

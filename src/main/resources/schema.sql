create table organizations
(
    id           bigint auto_increment PRIMARY KEY,
    name         varchar(255) not null,
    phone_number varchar(255) not null,
    address      varchar(255) not null,
    create_date  date default CURRENT_TIMESTAMP
);



create table users
(
    id                      bigint auto_increment PRIMARY KEY,
    username                varchar(255) not null,
    email                   varchar(255) not null,
    name                    varchar(255),
    surname                 varchar(255),
    password                varchar(255) not null,
    role                    varchar(255) not null,
    organization_id         bigint,
    create_date             date   default CURRENT_TIMESTAMP,
    credentials_non_expired bit(1) DEFAULT 1,
    account_non_expired     bit(1) DEFAULT 1,
    account_non_locked      bit(1) DEFAULT 1,
    active                  bit(1) DEFAULT 1,
    foreign key (organization_id) references organizations,
    UNIQUE KEY UK_USERNAME (username),
    UNIQUE KEY UK_EMAIL (email)
);


create table tasks
(
    id              bigint auto_increment PRIMARY KEY,
    title           varchar(255) not null,
    description     varchar(255) not null,
    deadline        date         not null,
    status          varchar(255) not null,
    organization_id bigint,
    create_date     date default CURRENT_TIMESTAMP,
    foreign key (organization_id) references organizations
);

create table users_tasks
(
    user_id bigint not null,
    task_id bigint not null,
    FOREIGN KEY (USER_ID) REFERENCES USERS,
    FOREIGN KEY (TASK_ID) REFERENCES TASKS
);

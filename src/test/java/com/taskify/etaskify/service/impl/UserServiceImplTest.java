package com.taskify.etaskify.service.impl;

import com.taskify.etaskify.exception.UserExistException;
import com.taskify.etaskify.model.dao.Organization;
import com.taskify.etaskify.model.dao.Role;
import com.taskify.etaskify.model.dao.User;
import com.taskify.etaskify.model.request.UserCreationRequest;
import com.taskify.etaskify.repository.UserRepository;
import com.taskify.etaskify.service.AvailabilityChecker;
import com.taskify.etaskify.service.OrganizationService;
import com.taskify.etaskify.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author : Orkhan Namazov
 * @since : 12.09.2021
 **/

@SpringBootTest
class UserServiceImplTest {
    private final int USER_COUNT = 5;
    @MockBean
    private Authentication authentication;
    @MockBean
    private UserRepository userRepository;

    @MockBean
    private AvailabilityChecker availabilityChecker;

    @MockBean
    private OrganizationService organizationService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserService userService;

    private Organization organization;
    private UserCreationRequest userCreationRequest;
    private List<User> userList = new ArrayList<>();


    @BeforeEach
    void setUp() {
        createOrganizationObj(1l, "organization_1", "994101000000", "demo_address");
        createUsrCreationObj("new user_1", "new user_s 1", "new_user_1@test.com", "123456");

        for (int i = 1; i < USER_COUNT; i++) {
            userList
                    .add(createUserObj(i,
                            "user_" + i,
                            "user_s" + i,
                            "123456",
                            1));
        }

    }

    @Test
    void getUsersById() {
        List<Long> ids = Arrays.asList(1l, 2l);
        Mockito.when(userRepository.findUsersById(ids)).thenReturn(userList);
        List<User> users = userService.getUsersById(ids);
        assertTrue(users == userList);
    }


    @Test
    void createUser() {
        Mockito.when(availabilityChecker.isEmailAvailable(userCreationRequest.getEmail())).thenReturn(true);
        Mockito.when(availabilityChecker.isUsernameAvailable(userCreationRequest.getUsername())).thenReturn(true);
        Mockito.when(userRepository.findByUsername(userCreationRequest.getUsername())).thenReturn(userList.get(1));
        Mockito.when(userRepository.insert(userList.get(1))).thenReturn(1);
        Mockito.when(organizationService.getOrganizationByUser()).thenReturn(organization);
        userService.createUser(userCreationRequest);

    }

    @Test
    void createUser_emailExists_Failure() {
        Mockito.when(availabilityChecker.isEmailAvailable(ArgumentMatchers.anyString())).thenThrow(UserExistException.class);
        assertThrows(UserExistException.class, () -> userService.createUser(userCreationRequest));
    }

    @Test
    void createUser_usernameExists_Failure() {
        Mockito.when(availabilityChecker.isUsernameAvailable(ArgumentMatchers.anyString())).thenThrow(UserExistException.class);
        assertThrows(UserExistException.class, () -> userService.createUser(userCreationRequest));
    }

    private User createUserObj(long id, String name, String surname, String password, long organizationId) {
        User temp = new User();
        temp.setId(id);
        temp.setName(name);
        temp.setSurname(surname);
        temp.setUsername(name);
        temp.setOrganizationId(organizationId);
        temp.setPassword(passwordEncoder.encode(password));
        temp.setRole(Role.ROLE_ADMIN);
        return temp;
    }

    private void createOrganizationObj(long id, String name, String phoneNumber, String address) {
        organization = new Organization(id, name, phoneNumber, address);
    }

    private void createUsrCreationObj(String name, String surname, String email, String password) {
        userCreationRequest = new UserCreationRequest();
        userCreationRequest.setUsername(name);
        userCreationRequest.setName(name);
        userCreationRequest.setSurname(surname);
        userCreationRequest.setPassword(password);
        userCreationRequest.setEmail(email);
    }


}
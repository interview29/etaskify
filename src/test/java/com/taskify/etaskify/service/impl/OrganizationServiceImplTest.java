package com.taskify.etaskify.service.impl;

import com.taskify.etaskify.model.dao.Organization;
import com.taskify.etaskify.model.dao.User;
import com.taskify.etaskify.repository.OrganizationRepository;
import com.taskify.etaskify.security.CustomUserDetailsService;
import com.taskify.etaskify.service.OrganizationService;
import com.taskify.etaskify.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * @author : Orkhan Namazov
 * @since : 12.09.2021
 **/

@SpringBootTest
class OrganizationServiceImplTest {
    @Autowired
    private OrganizationService organizationService;

    @MockBean
    private CustomUserDetailsService customUserDetailsService;

    @MockBean
    OrganizationRepository organizationRepository;

    private Organization organization;
    private User user;


    @BeforeEach
    void setUp() {
        organization = createDummyOrganization();
        user = createDummyUser(1l, "user1", "user0", 1l);
    }

    @Test
    void getOrganizationByUser() {
        Mockito.when(organizationRepository.getByUserId(user.getId())).thenReturn(organization);
        Mockito.when(customUserDetailsService.getUser()).thenReturn(user);
        Organization organizationByUser = organizationService.getOrganizationByUser();
        assertNotNull(organizationByUser);
        assertEquals(organization, organizationByUser);
    }


    private Organization createDummyOrganization() {
        Organization initial = new Organization();
        initial.setId(1l);
        initial.setName("Test");
        initial.setPhoneNumber("994994994994");
        initial.setAddress("Test Address");
        return initial;
    }

    private User createDummyUser(long id, String name, String surname, long organizationId) {
        User user = new User();
        user.setId(1l);
        user.setUsername(name + "_" + surname);
        user.setName(name);
        user.setSurname(surname);
        user.setOrganizationId(organizationId);
        return user;
    }
}
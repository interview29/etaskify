package com.taskify.etaskify.service.impl;

import com.taskify.etaskify.exception.TaskAssignException;
import com.taskify.etaskify.model.dao.*;
import com.taskify.etaskify.model.request.TaskCreationRequest;
import com.taskify.etaskify.repository.TaskRepository;
import com.taskify.etaskify.security.CustomUserDetailsService;
import com.taskify.etaskify.service.OrganizationService;
import com.taskify.etaskify.service.TaskService;
import com.taskify.etaskify.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

/**
 * @author : Orkhan Namazov
 * @since : 12.09.2021
 **/

@SpringBootTest
class TaskServiceImplTest {

    @Autowired
    private TaskService taskService;

    @MockBean
    private OrganizationService organizationService;

    @MockBean
    private TaskRepository taskRepository;

    @MockBean
    private CustomUserDetailsService userDetailsService;
    @MockBean
    private UserService userService;

    List<Task> tasks;
    Organization organization;
    User user;
    TaskCreationRequest taskCreationRequest;
    LocalDateTime now = LocalDateTime.now();

    @BeforeEach
    void setUp() {
        tasks = createTaskList();

        taskCreationRequest = new TaskCreationRequest();
        taskCreationRequest.setTitle("title 1");
        taskCreationRequest.setDeadline(now.plusDays(5));
        taskCreationRequest.setDescription("description 1");
        taskCreationRequest.setUserIds(Arrays.asList(1l, 2l, 3l));

        organization = new Organization();
        organization.setId(1l);
        organization.setPhoneNumber("994101111111");
        organization.setAddress("address 1");
        organization.setName("organization 1");

        user = new User();
        user.setId(10l);
        user.setName("name 10");
        user.setSurname("surname 10");
        user.setUsername("username 10");
        user.setOrganizationId(2);
        user.setRole(Role.ROLE_USER);

    }

    @Test
    void getTaskById() {
        when(taskRepository.getTaskById(1l)).thenReturn(tasks.get(0));
        Task taskById = taskService.getTaskById(1l);
        assertEquals(tasks.get(0), taskById);
    }

    @Test
    void createTask() {
        when(organizationService.getOrganizationByUser()).thenReturn(organization);
        when(taskRepository.createTask(tasks.get(0))).thenReturn(1);
        when(userService.getUsersById(ArgumentMatchers.anyList())).thenReturn(createUserList());
        taskService.createTask(taskCreationRequest);
    }

    @Test
    void assignTaskToUsers_success() {
        List<Long> userIds = Arrays.asList(1l, 2l, 3l);
        when(taskRepository.assignTaskToUser(tasks.get(0).getId(), userIds)).thenReturn(3);
        when(userService.getUsersById(userIds)).thenReturn(createUserList());
        int result = taskService.assignTaskToUsers(tasks.get(0), userIds);
        assertEquals(userIds.size(), result);
    }

    @Test
    void assignTaskToUsers_onlyExistedUsers() {
        List<Long> userIds = Arrays.asList(1l, 2l, 3l);
        when(taskRepository.assignTaskToUser(tasks.get(0).getId(), userIds)).thenReturn(2);
        when(userService.getUsersById(userIds)).thenReturn(createUserList());
        int result = taskService.assignTaskToUsers(tasks.get(0), userIds);
        assertEquals(2, result);
    }


    @Test
    void assignTaskToUsers_userNotExist_failure() {
        assertThrows(TaskAssignException.class, () -> taskService.assignTaskToUsers(tasks.get(0), null));
    }

    @Test
    void getOrganizationsTasks() {
        when(organizationService.getOrganizationByUser()).thenReturn(organization);
        when(taskRepository.getTasksOfOrganization(1l)).thenReturn(tasks.stream().filter(task -> task.getOrganizationId() == 1).collect(Collectors.toList()));
        List<Task> organizationsTasks = taskService.getOrganizationsTasks();
        assertEquals(4, organizationsTasks.size());
    }

    @Test
    void getUsersTasks() {
        when(userDetailsService.getUser()).thenReturn(user);
        when(taskRepository.getTasksOfUser(user.getId())).thenReturn(tasks.subList(3,4));
        List<Task> usersTasks = taskService.getUsersTasks();
        assertEquals(1, usersTasks.size());
    }

    private List<User> createUserList() {
        List<User> temp = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            User user = new User();
            user.setName("name " + i);
            user.setUsername("username " + i);
            user.setSurname("surname " + i);
            if (i % 2 == 0)
                user.setOrganizationId(1l);
            else
                user.setOrganizationId(2l);
            user.setRole(Role.ROLE_USER);
            user.setEmail("email." + i + "@test.com");
            temp.add(user);

        }
        return temp;
    }

    private List<Task> createTaskList() {
        List<Task> temp = new ArrayList<>();
        for (int i = 1; i < 6; i++) {
            temp.add(
                    new Task(i,
                            "title " + i,
                            "description " + i,
                            now.plusDays(5),
                            TaskStatus.NEW,
                            1));
        }
        temp.get(4).setOrganizationId(2);

        return temp;
    }



}